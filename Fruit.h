#pragma once

#include <iostream>
#include <string>
#include <memory>
using namespace std;

enum class CompareMode {
  NAME,
  PRICE,
  WEIGHT,
  COLOR,
  DEFAULT
};

// TODO: give derived class for each kind of fruit.
//       Revise the following base class and add new derived classes.
// TODO: Overload the arithmetic operators +,-,*,/, +=, -=, *=, /=, ++, --, and ** for Fruits.
// 
class Fruit {
  friend std::ostream &operator<< (std::ostream &out, const Fruit *r) ;

  std::string mname;
  
  int  munitPrice;
  int  mweight;
  size_t mid;

  std::string mColor;

public:
  Fruit () = default;
  Fruit ( const string& , int , int , size_t , const std::string& ) ; 
  Fruit (const string& name, int p, int w, size_t id) ;

  // overload operator <, then  we can compare Fruits (i.e., we can compare orange and apple)
  bool operator< (const Fruit &o) const ;
  int getWeight() const  ;
  const string& getName() const  ;
  int getPrice() const  ;
  int getId() const  { return mid; } ;

  const string& getColor() const  ;

  static string getModeName( CompareMode) ;
  string getPropertyValue( CompareMode) const ;

};
