#pragma once

#include <iostream>
#include <string>
#include <memory>
#include <iterator>
#include <algorithm>

#include "Fruit.h" 

using namespace std;




struct FruitCompare {
  CompareMode mmode ;

  FruitCompare ( ) = default ;
  FruitCompare (CompareMode m ) { setCompareMode (m); }


  static string modeName ( CompareMode m) {
    vector<string> names { "NAME", "PRICE", "WEIGHT", "COLOR","DEFAULT" };
    return names[ static_cast<size_t> ( m ) ] ; // xxx
  }
  string modeName ( ) const {
    return modeName ( mmode );
  }

  bool operator() (const Fruit* x, const Fruit* y) const ;
  bool operator() (const unique_ptr<Fruit>& x, const unique_ptr<Fruit>& y) const ;

  void setCompareMode ( CompareMode m ) {
    mmode =  m;
  }
};
static CompareMode mmode = CompareMode::DEFAULT;

class Vendor {
private:
  friend std::ostream & operator<< (std::ostream &out, const Vendor& r) ;

  void sortWithFunctor( CompareMode m) ;

  std::string mname;
  vector < unique_ptr<Fruit> > mfruit;

public:
  void sortWithoutFunctor( CompareMode m) ;
  void sortDB ( CompareMode mode, bool withFunctor = true )  ;
  
  Vendor (const string& ) ;
  void collectFruit ( size_t ) ;
  int getTotalPrice ( )  const ;

public:
  void reportDB ( const string& )  const ;
  void reportDB( CompareMode mode, const string& fn, bool inverse_order ) const ;
  void reportDB( CompareMode mode, ofstream& fout, bool inverse_order ) const ;

  void readDB ( const string& )  ;
  void readBinaryDB ( const string& )  ;

  void saveDB ( const string& )  const ;

  void saveBinaryDB ( const string& )  const ;

};
