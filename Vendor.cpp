#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <set>
#include <map>
#include <memory>
#include <numeric>



#include "Vendor.h"

using namespace std;


  // overload operator< as a global function as a member function
  // bool operator< (const Fruit& x, const Fruit& y ) {
    // return x.getId() > y.getId() ;
  // }

  // Overload insertion operator << for Fruit
  // This overloaded function must be a friend of Fruit 
  // since it will access the private member data of Fruit 

  std::ostream & operator<< (std::ostream &out, const Fruit *r) {

    string id = to_string (r->mid) ;
    string p = "$" + to_string (r->munitPrice) + "/oz" ;
    string s = to_string(r->mweight) + "oz" ;
    out << setw(10) << id << setw(15) << r->mname << setw(15) << p 
    << setw (15) << s ;
    s = "$" + to_string(r->getPrice()) ;
    out << setw (15) << s ;
    s = r->getColor() ;
    out << setw (15) << s ;
    return out;
  }

  std::ostream & operator<< (std::ostream &out, const unique_ptr<Fruit> & r) {
    out << r.get();    
    return out;
  }

  // Overload insertion operator << for Fruit
  // This overloaded function must be a friend of Fruit 
  // since it will access the private member data of Fruit 

  std::ostream & operator<< (std::ostream &out, const Vendor& r) {
    out << "#" << setw(10) << "ID" << setw(15) << "Name" << setw(15) \
      << "Unit_Price" << setw(15) \
      << "Weight(oz)" <<  setw(15) << "Total_Price" << setw(15) << "Color" << endl;
    string s (10, '-');
    out << "#" ;
    for (size_t i=0; i < 6; ++i ) {
      out << s << setw(15) ;
    }
    out << endl;
    for ( auto& f : r.mfruit ) {
      out << f << endl; 
    }
    out << "#Total price: $" << r.getTotalPrice () << endl;;
    return out;
  }



  void Vendor::collectFruit ( size_t tot ) {
    vector<string> names { "apple", "orange", "banana", "apricot", "kiwi", "papaya" };
    vector<string> colors { "red", "blue", "pink", "yellow", "green" };

    for (size_t i=0; i < tot; ++i ) {
      size_t c = rand()%colors.size(); // c is random index to array collors
      size_t j = rand()%names.size(); // j is the random index to array names 
      int p = rand()%3+1;             // p is a random unit price between $1 and $3
      int w = rand()%5+1;             // w is a random weight between 1 and 5 (oz)
      Fruit* q ;

      q = new Fruit( names[j], p,w, mfruit.size(), colors[c])  ; 
      mfruit.emplace_back ( q );
    }
    cout << mname << " collects " << tot << " new fruit" << endl;

  }

  Vendor::Vendor ( const string& name ) : mname(name) 
  {
  }

  int Vendor::getTotalPrice ( )  const {
    return std::accumulate( mfruit.begin(),  mfruit.end(), 0, 
                                    [](int a, const unique_ptr<Fruit>& b ) {
                                        return a + b->getPrice() ;
                                    });
  }

bool comparePrice(const unique_ptr<Fruit>& x, const unique_ptr<Fruit>& y) {
    return x->getPrice() < y->getPrice();
}
bool compareWeight(const unique_ptr<Fruit>& x, const unique_ptr<Fruit>& y) {
    return x->getWeight() < y->getWeight();
}
bool compareName(const unique_ptr<Fruit>& x, const unique_ptr<Fruit>& y) {
    return x->getName() < y->getName();
}
bool compareColor(const unique_ptr<Fruit>& x, const unique_ptr<Fruit>& y) {
    return x->getColor() < y->getColor();
}

bool compareDefault(const unique_ptr<Fruit>& x, const unique_ptr<Fruit>& y) {
    return  *x < *y ;
}

// This is the key comparison function for sorting fruits


bool FruitCompare::operator() (const unique_ptr<Fruit>& x, const unique_ptr<Fruit>& y) const 
{
  switch ( mmode ) {
    case CompareMode::PRICE:
      return x->getPrice() < y->getPrice();
      break;
    case CompareMode::WEIGHT:
      return x->getWeight() < y->getWeight();
      break;
    case CompareMode::NAME:
      return x->getName() < y->getName();
      break;
    case CompareMode::COLOR:
      return x->getColor() < y->getColor();
      break;
    case CompareMode::DEFAULT:
      return *x < *y; 
      break;
    default:
      return *x < *y; 
      break;
  };
}

void Vendor::sortDB ( CompareMode mode, bool withFunctor )  {
  if ( withFunctor ) {
    sortWithFunctor ( mode)  ;
  } else {
    sortWithoutFunctor ( mode)  ;
  }
}

void Vendor::sortWithFunctor ( CompareMode mode)  {
  // a functor that compares fruits based on the given comparison mode
  FruitCompare myCompare (mode) ; 

  string name = myCompare.modeName();

  cout << mname << " sorts fruits by " << name << " using functor." << endl;
  std::sort ( mfruit.begin(), mfruit.end(), myCompare );
}


void Vendor::sortWithoutFunctor( CompareMode m) 
{
  string name = FruitCompare::modeName(m );

  cout << mname << " sorts fruits by " << name << " w/o functor." << endl;
  switch ( m ) {
    case CompareMode::PRICE:
      std::sort ( mfruit.begin(), mfruit.end(), comparePrice );
      break;
    case CompareMode::WEIGHT:
      std::sort ( mfruit.begin(), mfruit.end(), compareWeight );
      break;
    case CompareMode::NAME:
      std::sort ( mfruit.begin(), mfruit.end(), compareName );
      break;
    case CompareMode::COLOR:
      std::sort ( mfruit.begin(), mfruit.end(), compareColor );
      break;
    case CompareMode::DEFAULT:
      std::sort ( mfruit.begin(), mfruit.end(), compareDefault );
      break;
    default:
      std::sort ( mfruit.begin(), mfruit.end() );
      break;
  };
}


void Vendor::saveDB( const string& fn) const
{
  ofstream fout (fn);
  cout << "saveDB: file " << fn << endl;
  
  fout << *this << endl;
  fout.close();
}


void foo ( int f, set<string> myset, ofstream& fout ) {
  fout << f << " times. It includes  " ;
  for ( const auto& s: myset ) {
    fout << s << ", "     ;
  }
  fout << endl;
}

// study STL container "vector" at http://www.cplusplus.com/reference/vector/vector/vector/
// study STL container "map" at http://www.cplusplus.com/reference/map/map/map/
// study STL container "set" at http://www.cplusplus.com/reference/set/set/set/

// TODO: Enhance data analysis of member function reportDB with both histogram and inverse 
// histograms for each fruit property like names, color, weight, ....  
// A sample report file is given as part of the starter programs.
// Hints: use STL containers set and map
void Vendor::reportDB( CompareMode mode, ofstream& fout, bool inverse_order ) const {
}

	void Vendor::reportDB(const string& fn ) const {
    size_t cnt = mfruit.size();
    cout << "Report DB: file=" <<  fn << ", " << cnt << " entries." << endl;
    ofstream out (fn);
    string msg1 = 
    "# Histogram shows the frequency of a certain fruit property." ;
    string msg2 = 
    "# Inverse Histogram lists sets of attribute values with the same frequency.";
    out << msg1 << endl;
    out << msg2 << endl;
    out << endl ; 

    for (CompareMode i=CompareMode::NAME; i <= CompareMode::DEFAULT; ) {
      reportDB ( i, out, false );
      i = static_cast <CompareMode> ( (int) i+1) ;
    }

    out.close();
  }  


// TODO: finish it as you did in lab #B
void Vendor::saveBinaryDB( const string& fn) const {
  ofstream fout (fn);
  cout << "saveBinaryDB: file (To be finished)" << fn << endl;
  cout << "saveBinaryDB: To be finished" << endl;
  
  fout.close();
}

int extractInteger ( const string& s ) {
    string temp ;
    for ( auto& ch: s ) {
      if ( isdigit ( ch ) ) {
        temp += ch;
      }
    }
    return stoi(temp) ;
}

// The following is a special range constructor for STL container vector

// see details at http://www.cplusplus.com/reference/vector/vector/vector/
//  and http://www.cplusplus.com/reference/iterator/istream_iterator/

vector<string> tokenizeStringUsingRangeCstr ( const string& line ) 
{
  istringstream iss( line ); // convert string line to a stream object

  istream_iterator<string> itr {iss} ;// stream iterator
  istream_iterator<string> eos {};  // end-of-stream iterator

  vector<string> tokens { itr, eos } ;
  return tokens;
}

void tokenizeString ( const string& line, vector<string>& tokens ) 
{
    istringstream iss( line ); // convert string line to a stream object
    string temp ;
    while ( iss >> temp ) {
      tokens.push_back ( temp );
    }
}

// TODO: When reading DB files, report corrupted data using Exception handling instead of 
// simply crash or quit silently.

void Vendor::readDB( const string& fn) {
  ifstream fin (fn);
  string line ;
  cout << "ReadDB: file " << fn << endl;

  size_t cnt = 0  ;
  size_t duplicate = 0  ;

  // we use STL container "set" to detect a duplicate fruit id
  // see details and examples at http://www.cplusplus.com/reference/set/set/set/
  set<size_t>myset ;
  for (const auto& p: mfruit ) {
    myset.insert ( p->getId() ) ; // insert  all fruit id in current DB to myset 
  }
  while  (getline ( fin, line ) ) {

    vector<string> tokens ;
    tokenizeString ( line, tokens );

    size_t n = tokens.size();
    if (n < 1 )  continue;
    if ( tokens[0][0] == '#' ) continue;
    // cout << line << endl;

    size_t id = stoi ( tokens[0] );
    if ( myset.find(id) == myset.end() ) {
      const string& species = tokens[1]; 
      int p = extractInteger ( tokens[2] );
      int w = extractInteger ( tokens[3] );
      string color = tokens[5];
    
      Fruit* q = new Fruit( species, p,w, id, color );
      mfruit.emplace_back ( q );
      cnt++;
      myset.insert ( id );
    } else {
      duplicate++;
    }
  }
  cout << "------> read " << cnt + duplicate << " entries." << endl; 
  if ( duplicate > 0 ) {
    cout << "------> find " << duplicate << " duplicate entries." << endl; 
  }
  fin.close();
}
