#include <iostream>
#include <iomanip>
#include <vector>
#include <set>
#include <memory>

#include "Vendor.h"

using namespace std;



void unitTest ( Vendor& vendor ) 
{

  size_t last = static_cast< size_t > (CompareMode::DEFAULT); 

  for (int i=0; i <= last; ++i ) 
  { 
    CompareMode m = static_cast< CompareMode >(i) ;
    vendor.sortDB (m, i%2==0); 
    // cout << vendor << endl;
  }

  vendor.sortDB ( CompareMode::NAME ); 
  vendor.saveDB ( "db3.txt" );
}

int main () 
{
  Vendor vendor("John" );
  // vendor.collectFruit ( 1000 ); vendor.saveDB ( "db1.txt" );
  // unitTest (vendor );

  vendor.readDB ( "db1.txt" );
  vendor.readDB ( "db2.txt" );

  vendor.reportDB ( "report.txt" );

  vendor.sortDB (CompareMode::DEFAULT, true ); 
  vendor.saveDB ( "db3.txt" );
  vendor.saveBinaryDB ( "db3.dat" );
}
